/**
 * 
 */
package org.mampu.mongodb.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.mampu.mongodb.dal.AttachmentRepository;
import org.mampu.mongodb.dal.ProfileRepository;
import org.mampu.mongodb.model.Attachment;
import org.mampu.mongodb.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */

@SpringBootApplication
@EntityScan(basePackages = { "org.mampu.mongodb.model" })
@EnableMongoRepositories(basePackages = {"org.mampu.mongodb.dal"})
@EnableJpaRepositories(basePackages = { "org.mampu.mongodb.dal" })
public class Application implements CommandLineRunner {
	
	@Autowired
	AttachmentRepository attachmentRepo;
	
	@Autowired
	ProfileRepository profileRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		System.out.println("Start uploading attachment...");
		
		// The attachment folder name
		String folder = "/Users/shaiful/Downloads/";
		
		// Read all the record in Profile table
		List<Profile> profileList = profileRepo.findAll();
		
		// For every profile, upload the attachment into the Mongo Db
		for(Profile profile : profileList) {
			
			// Read the file from hard disk
			String fileName = folder + profile.getFilename();
			System.out.println("Processing file " + fileName);
			File file = new File(fileName);
			
			// Read the file into byte array
			byte[] bytesArray = new byte[(int)file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray);
			fis.close();
			
			// Convert the byte array into BSON format
			Binary bin = new Binary(BsonBinarySubType.BINARY, bytesArray);
			
			// Create new Attachment Document
			Attachment attach = new Attachment();
			attach.setDocumentId(profile.getFilename());
			attach.setOriginalName(profile.getOriginalname());
			attach.setCreationDate(new Date());
			attach.setContent(bin);
			
			// Save the document into MongoDb
			attachmentRepo.save(attach);
			
			System.out.println("Completed.");
		}
		
		// Read one of the Attachment from MongoDb
		Attachment attach = attachmentRepo.findByDocumentId("800728115712_1503023832082.pdf");
		System.out.println("Original File Name : " + attach.getOriginalName());
		
		// Write back the file to hard disk
		byte [] fileContent = attach.getContent().getData();
		FileOutputStream fout = new FileOutputStream(folder + attach.getOriginalName());		
		fout.write(fileContent);
        fout.flush();
        fout.close();
        
	}
}
