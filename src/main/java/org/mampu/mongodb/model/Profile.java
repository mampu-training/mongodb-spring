/**
 * 
 */
package org.mampu.mongodb.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */

@Entity
@Table(name = "profile", schema = "public")
public class Profile {

	@Id
	private Integer id;
	
	private String filename;
	
	private String originalname;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the originalname
	 */
	public String getOriginalname() {
		return originalname;
	}

	/**
	 * @param originalname the originalname to set
	 */
	public void setOriginalname(String originalname) {
		this.originalname = originalname;
	}
	
	
}
