/**
 * 
 */
package org.mampu.mongodb.dal;

import org.mampu.mongodb.model.Attachment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */

@Repository
public interface AttachmentRepository extends MongoRepository<Attachment, String> {
	
	public Attachment findByDocumentId(String documentId);
	
}
