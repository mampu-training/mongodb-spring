/**
 * 
 */
package org.mampu.mongodb.dal;

import org.mampu.mongodb.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */
public interface ProfileRepository extends JpaRepository<Profile, Integer> {

}
