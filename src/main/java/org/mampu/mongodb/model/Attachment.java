/**
 * 
 */
package org.mampu.mongodb.model;

import java.util.Date;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */

@Document
public class Attachment {

	@Id 
	@Field
	private String documentId;
	
	@Field
	private String originalName;
	
	@Field
	private Binary content;
	
	
	@Field
	private Date creationDate = new Date();


	/**
	 * @return the documentId
	 */
	public String getDocumentId() {
		return documentId;
	}


	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	/**
	 * @return the originalName
	 */
	public String getOriginalName() {
		return originalName;
	}


	/**
	 * @param originalName the originalName to set
	 */
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}


	/**
	 * @return the content
	 */
	public Binary getContent() {
		return content;
	}


	/**
	 * @param content the content to set
	 */
	public void setContent(Binary content) {
		this.content = content;
	}


	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
}
